import {LitElement, html, css} from 'lit-element';
import i18next from 'i18next';
import * as i18n from './i18n/index';

export class MasterMessage extends LitElement {
  static get properties() {
    return {
      name: {type: String},
      locale: {type: String}
    };
  }

  static get styles() {
    return css`
      div { border: 1px solid red; color: red; }
    `;
  }

  constructor() {
    super();
    this.localize();
    this.name = '';

  }

  localize()
  {
    i18next.init({
      lng: 'en',
      debug: true,
      resources: {
        en: {
          translation: i18n.en_US
        },
        jp: {
          translation: i18n.jp_JP
        }
      }
    });
  }

  // Render element DOM by returning a `lit-html` template.
  render() {
    const content = {
      title: i18next.t('title'),
      implementedWithText: i18next.t('implementedWithText', {link: `<a href="https://lit-element.polymer-project.org/guide">$t(implementedWithLinkLabel)</a>`}),
      parameterPassedText: i18next.t('parameterPassedText', {name: this.name}),
      subSectionHeader: i18next.t('subSectionHeader'),
      implementationTimeText: i18next.t('implementationTime', {days: 3}),
      clearNotificationsButtonLabel: i18next.t('clearNotificationsButtonLabel'),
      displayMessageButtonLabel: i18next.t('displayMessageButtonLabel')
    };

    const output = html`
    <h3>${content.title}</h3>

    ${content.implementedWithText}<br/>
    ${content.parameterPassedText}<br/>

    <h4>${content.subSectionHeader}</h4>

    ${content.implementationTimeText}

    <button @click="${this.primaryClickHandler}">${content.clearNotificationsButtonLabel}</button>
    <button @click="${this.secondaryClickHandler}">${content.displayMessageButtonLabel}</button>
    `;

    return output;
  }


  primaryClickHandler()
  {
    let bubbles = true;
    let cancelable = true;
    let customEventDetails = {};

    let evt = document.createEvent('CustomEvent');
    evt.initCustomEvent("mastermessage_primary_click", bubbles, cancelable,  customEventDetails);
    this.dispatchEvent(evt);

    i18next.changeLanguage(i18next.language === "en" ? "jp" : "en");

    this.update();
  }


  secondaryClickHandler()
  {
    let bubbles = true;
    let cancelable = true;
    let customEventDetails = {};

    let evt = document.createEvent('CustomEvent');
    evt.initCustomEvent("mastermessage_secondary_click", bubbles, cancelable,  customEventDetails);
    this.dispatchEvent(evt);
  }
}

customElements.define("master-message", MasterMessage);
