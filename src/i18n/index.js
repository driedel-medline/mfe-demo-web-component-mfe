import * as en_US from './en_US.json';
import * as jp_JP from './jp_JP.json';

export { en_US, jp_JP };