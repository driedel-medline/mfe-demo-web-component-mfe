const HtmlWebpackPlugin = require('html-webpack-plugin');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const CopyPlugin = require('copy-webpack-plugin');
const path = require('path');

module.exports = {
  entry: './src/index',
  cache: false,

  mode: 'development',
  devtool: 'source-map',

  optimization: {
    minimize: false
  },

  output: {
    publicPath: 'http://localhost:2200/'
  },

  resolve: {
    extensions: ['.js', '.json']
  },

  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: [path.resolve(__dirname, 'node_modules')],
        include: [path.resolve(__dirname, "src")],
        use: [
          { loader: 'babel-loader' }
        ]
      }
    ]
  },

  plugins: [
    new ModuleFederationPlugin({
      name: 'masterMessage',
      library: { type: 'var', name: 'masterMessage' },
      filename: 'remoteEntry.js',
      exposes: {
    	  './MasterMessage': './src/index'
      },
      shared: [
        "@webcomponents/webcomponentsjs",
        "lit-element"
      ]
    }),
    new HtmlWebpackPlugin({
      template: './public/index.html'
    }),
  ]
};
